﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework_01
{
    class Program
    {
        /// <summary>
        /// Код, который выполняется в первую очередь
        /// </summary>
        /// <param name="args">Параметры запуска</param>
        static void Main(string[] args)
        {
            // Задание один

            Console.WriteLine("Задание 1");

            // Создание базы данных из 19 сотрудников
            Repository repositoryPrimus = new Repository(19);

            // Печать в консоль всех сотрудников
            repositoryPrimus.Print("База данных до преобразования");

            // Увольнение всех работников с именем Агата
            repositoryPrimus.DeleteWorkerByName("Агата");

            // Печать в консоль сотрудников, которые не попали под увольнение
            repositoryPrimus.Print("База данных после преобразования");

            // Задание два

            Console.WriteLine("Задание 2");

            // Создание базы данных из 40 сотрудников
            Repository repositorySecundus = new Repository(40);

            // Печать в консоль всех сотрудников
            repositorySecundus.Print("База данных до преобразования");

            // Волны увольнения
            do
            {
                string imposter = repositorySecundus.Workers.ElementAt(0).FirstName;
                Console.WriteLine($"Уволить: {imposter}");
                repositoryPrimus.DeleteWorkerByName(imposter);

                // Печать в консоль сотрудников, которые не попали под увольнение
                repositoryPrimus.Print("База данных после преобразования");
            } while (repositoryPrimus.Workers.Count > 30);

            // Задание три

            Console.WriteLine("Задание 3");

            // Создание базы данных из 50 сотрудников
            Repository repositoryTertius = new Repository(50);

            // Печать в консоль всех сотрудников
            repositoryTertius.Print("База данных до преобразования");

            // Увольнение всех работников с зарплатой больше 30 000 рублей
            repositoryTertius.DeleteWorkerBySalary(30000);

            // Печать в консоль сотрудников, которые не попали под увольнение
            repositoryTertius.Print("База данных после преобразования");

            #region Домашнее задание

            // Уровень сложности: просто
            // Задание 1. Переделать программу так, чтобы до первой волны увольнения в отделе было не более 20 сотрудников

            // Уровень сложности: средняя сложность
            // * Задание 2. Создать отдел из 40 сотрудников и реализовать несколько увольнений, по результатам
            //              которых в отделе должно остаться не более 30 работников

            // Уровень сложности: сложно
            // ** Задание 3. Создать отдел из 50 сотрудников и реализовать увольнение работников
            //               чья зарплата превышает 30000руб



            #endregion


            Console.ReadKey();
        }
    }
}
